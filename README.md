# GScript --- Project Proposal
*The new innovative way to make programming platforms for educational use.*
## Introduction
There are many games out there that introduces the ordinary person to programming concepts or just programming logic or just plain logic for that matter. What these games lack is the ability to scale: the ability to allow the player to get from playing said game to be able to start coding in a proper high level programming language. The problem also exists with when the user finishes the said game and they can't play anymore because these programming type games tend not to have replay-ability and their "boards" / "levels" gets completed and there is nothing left to do. Another issue that arises with these games is that since most of them are deployed to mobile devices there are uncomfortable screen and keyboard constraints. This problem relates to it being difficult to do actual programming on a mobile device. This is usually why there is this big gap between programming games and high level programming.

## Scope
Before we look into what we are going to do let's see what the goals and milestones are that we would like to achieve:
There will be 3 levels to the scope of this project.
### Level 1: Main objectives
- Define Life-cycles
	- Object
	- System
	- Component *(More on how this injects into the life-cycles)*

- Declare objects that we will refer to as
	 - Entities -- Your general objects that represent real world objects.
	 - Environment -- This will be the level or board, scene.
	 - Eventities -- The events that are triggered during an object's life cycle to be reconfigured.
 
- Define functions that will handle:
	 - The movements of the entity.
	 - The actions of the entity
	 - The reactions of the entity
	 - The general behaviour of the entity
	 - The senses of the entity
	 - The appearance of the entity

- Define functions that will handle
	- The visuals of the environment
	- The transitions between environments
	- Constraints that limit factors inside of said environment
	- Ambient effects in environment
### Level 2: 

### Level 3: 
- Library must be accessible on any platform.
 - Code injection for instructions that are out of the API's scope.

## Project description
The objective is to create a library that any person(Dev) with the ability to:
- Create assets
- Create a GUI
- Create own IO
- Link IO to API calls

This Dev should be able to make a game using the library. In general, the game's aim should be to teach the player programming. or at least logic about how some set of instructions can result in some actions and then with minor adjustments can lead to different results.

Taking the mentioned goals in mind and the Dev requirements . The Dev should be able to make use of the api to build a game that could teach programming from as low as basic Entity movements to Complex entity instructions to as high as Object inheritance and polymorphism. 

It is to be noted that the Dev does not have to implement all the features that api allows.
It is also to be noted that it is advised for the Dev to also make a level designer that the player can use to extend their ingame time.

## Inspiration List
This Library is highly inspired by [Scratch](https://scratch.mit.edu)
