function rgbTOhex(rgb) {
    let fullColorHex = (r, g, b) => {
        let red = rgbToHex(r);
        let green = rgbToHex(g);
        let blue = rgbToHex(b);
        return red + green + blue;
    };

    let rgbToHex = (rgb) => {
        let hex = Number(rgb).toString(16);
        if (hex.length < 2) {
            hex = "0" + hex;
        }
        return hex;
    };

    let NEWrbg = rgb.split('(')[1].split(')')[0].split(',');
    return "#" + fullColorHex(NEWrbg[0], NEWrbg[1], NEWrbg[2]);
}

