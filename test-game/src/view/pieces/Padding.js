class Padding {
    top = 0;
    bot = 0;
    left = 0;
    right = 0;

    constructor(top, bot, left, right) {
        this.top = top;
        this.bot = bot;
        this.left = left;
        this.right = right;
    }

    setPadding(val) {
        this.top = val;
        this.bot = val;
        this.left = val;
        this.right = val;
        return this;
    }

    setVpadding(top, bot) {
        this.top = top;
        this.bot = bot;
        return this;
    }

    setHpadding(left, right) {
        this.left = left;
        this.right = right;
        return this;
    }

    setVHpadding(vPad, hPad) {
        this.top = vPad;
        this.bot = vPad;
        this.left = hPad;
        this.right = hPad;
        return this;
    }

    h() {
        return this.left + this.right;
    }

    v() {
        return this.top + this.bot;
    }

    clone() {
        return new Padding(this.top, this.bot, this.left, this.right);
    }
}