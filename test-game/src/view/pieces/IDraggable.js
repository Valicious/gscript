class IDraggable {
    constructor(){
        if (this.constructor === IDraggable) {
            throw new TypeError("Can not construct class.");
        }
        if (this.drag === IDraggable.prototype.drag) {
            throw new TypeError("Please implement method drag.");
        }
    }

    drag(mouseX, mouseY){
        throw new TypeError("Do not call abstract method drag from child.");
    }
}