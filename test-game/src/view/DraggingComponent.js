const opacityOffsetHex = 90;

class DraggingComponent {
    xOffset;
    yOffset;
    uiComp;
    oldColour;

    constructor(uiComp, xOffset, yOffset) {
        this.uiComp = uiComp;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.oldColour = this.uiComp.colour;
        if (this.uiComp.colour.charAt(0) === "#") { //hex
            this.uiComp.colour += opacityOffsetHex;
        } else {
            this.uiComp.colour = rgbTOhex(this.uiComp.colour) + opacityOffsetHex;
        }
    }

    moveToXY(posX, posY) {
        this.uiComp.calculatedXpos = posX;
        this.uiComp.calculatedYpos = posY;
    }

    draw() {
        fill(this.uiComp.colour);
        rect(this.uiComp.calculatedXpos - this.xOffset, this.uiComp.calculatedYpos - this.yOffset, this.uiComp.dx, this.uiComp.dy, this.uiComp.radius);
    }

    getUIcomp(){
        this.uiComp.colour = this.oldColour;
        return this.uiComp;
    }
}

let curDraggingElm = undefined;
let isDragging = false;

function setDraggingComp(uiComp, xOffset, yOffset) {
    return new DraggingComponent(uiComp.clone(), xOffset, yOffset);
}

function drawUIDragging() {
    if (curDraggingElm) {
        curDraggingElm.draw();
    }
}

function mouseReleased() {
    if (curDraggingElm) {
        for (let comp in UIComponents) {
            if (UIComponents[comp].dropComponent(mouseX, mouseY, curDraggingElm)) break;
        }
    }
    curDraggingElm = undefined;
    isDragging = false;
}

function mouseDragged() {
    if (curDraggingElm)
        curDraggingElm.moveToXY(mouseX, mouseY);
    else if (!isDragging) {
        isDragging = true;
        for (let comp in UIComponents) {
            if (UIComponents[comp].drag(mouseX, mouseY)) break;
        }
    }
}

function isMouseInArea(comp, mouseX, mouseY) {
    //Find what the mouse is dragging
    // check if mouse is in box
    // within x-axis
    if (mouseX >= comp.calculatedXpos && mouseX <= comp.calculatedXpos + comp.calculatedDx) {
        //within y-axis
        if (mouseY >= comp.calculatedYpos && mouseY <= comp.calculatedYpos + comp.calculatedDy) {
            //mouse is in box
            return true;
        }
        //else do nothing
    }
    //else do nothing
    return false
}

