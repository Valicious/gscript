const _defaultBackgroundColour = "#72ff8d";

class UIContainer extends UIComponent {
    xPos;
    yPos;

    children = [];
    dropzone = undefined;
    padding = new Padding(0, 0, 0, 0);
    childrenMatchConstraint = false;
    wrapChildren = false;


    constructor(xPos, yPos, dx, dy, radius, colour) {
        super(dx, dy, radius, colour);
        this.xPos = xPos;
        this.yPos = yPos;
        if (this.constructor === UIContainer) {
            throw new TypeError("Can not construct class.");
        }
        if (this.addChild === UIContainer.prototype.addChild) {
            throw new TypeError("Please implement method addChild.");
        }
        if (this.draw === UIContainer.prototype.draw) {
            throw new TypeError("Please implement method draw.");
        }
        if (this.minimizeD === UIContainer.prototype.minimizeD) {
            throw new TypeError("Please implement method minimizeD.");
        }
    }

    addChild(uiComp) {
        throw new TypeError("Do not call abstract method addChild from child.");
    }

    draw(offset) {
        throw new TypeError("Do not call abstract method draw from child.");
    }

    minimizeD(uiComp) {
        throw new TypeError("Do not call abstract method minimizeD from child.");
    }

    drag(mouseX, mouseY) {
        if (isMouseInArea(this, mouseX, mouseY)) {
            //check overlay/child first
            if (this.children.length > 0) {
                for (let index in this.children) {
                    let comp = this.children[index];
                    if (comp.drag(mouseX, mouseY)) {
                        return true;
                    }
                }
            }
            //no overlay/child found
            if (this.movable) {
                curDraggingElm = setDraggingComp(this, mouseX - this.calculatedXpos, mouseY - this.calculatedYpos);
            }
            return true;
        }
    }

    setDropzone(dropzone) {
        this.dropzone = dropzone;
        this.dropzone.setBoundingBox(this);
    }

    dropComponent(mouseX, mouseY, dragItem) {
        if (this.dropzone && isMouseInArea(this, mouseX, mouseY)) {
            this.dropzone.dropComponent(dragItem);
            return true;
        }
        return false;
    }

    clone() {
        let newContainer = new UIContainer(this.xPos, this.yPos, this.dx, this.dy, this.radius, this.colour);
        this.children.forEach(curChild => {
            newContainer.children.push(curChild.clone());
        });
        /*if (this.dropzone)
            newContainer.dropzone = this.dropzone.clone();*/
        newContainer.padding = this.padding.clone();
        newContainer.setCalculatedPosition(this.calculatedXpos, this.calculatedYpos, this.calculatedDx, this.calculatedDy);
        return newContainer;
    }

}