class VBox extends UIContainer {

    constructor(xPos, yPos, dx, dy, radius, colour) {
        super(xPos, yPos, dx, dy, radius, colour);
        this.calculatedXpos = this.xPos;
        this.calculatedYpos = this.yPos;
        this.calculatedDx = this.dx;
        this.calculatedDy = this.dy;
        this.minDY = this.dy;
        this.minimizeD(this);
    }

    addChild(uiComp) {
        let newComp = uiComp.clone();
        this.children.push(newComp);
        this.minimizeD(newComp)
    }

    minimizeD(uiComp) {
        //make sure the container can fit in the new component. requirement!
        let allocationDX = this.padding.h() + uiComp.dx;
        if (allocationDX > this.dx) this.dx = allocationDX;
        //recalculate container height
        let allocationDY = this.padding.v();
        this.children.forEach(curChild => {
            allocationDY += curChild.dy + this.justifyChildren;
        });
        allocationDY -= this.justifyChildren;
        this.dy = allocationDY < this.minDY ? this.minDY : allocationDY;
    }

    draw(offsetY = 0) {
        //draw this
        stroke(0);
        fill(this.colour === undefined ? _defaultBackgroundColour : this.colour);

        this.calculatedXpos = this.xPos;
        this.calculatedYpos = this.yPos;
        this.calculatedDx = this.dx;
        this.calculatedDy = this.dy;

        rect(this.xPos, this.yPos, this.dx, this.dy, this.radius);

        let xPos = this.xPos + this.padding.left;
        let yPos = this.yPos + this.padding.top;
        //draw children
        for (let i = 0; i < this.children.length; i++) {

            /*this.children[i].draw(xPos, yPos + (this.justifyChildren + this.children[i === 0 ? i : i - 1].dy) * i );*/
            this.children[i].draw(xPos, yPos + (this.justifyChildren * i));
            yPos += this.children[i].dy;
        }
    }
}