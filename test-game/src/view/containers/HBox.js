class HBox extends UIContainer {

    constructor(xPos, yPos, dx, dy, radius, colour) {
        super(xPos, yPos, dx, dy, radius, colour);
        this.calculatedXpos = this.xPos;
        this.calculatedYpos = this.yPos;
        this.calculatedDx = this.dx;
        this.calculatedDy = this.dy;
        this.minDX = this.dx;
        this.minimizeD(this);
    }

    addChild(uiComp) {
        let newComp = uiComp.clone();
        this.children.push(newComp);
        this.minimizeD(newComp)
    }

    minimizeD(uiComp) {
        //make sure the container can fit in the new component. requirement!
        let allocationDY = this.padding.v() + uiComp.dy;
        if (allocationDY > this.dy) this.dy = allocationDY;
        //recalculate container height
        let allocationDX = this.padding.h();
        this.children.forEach(curChild => {
            allocationDX += curChild.dx + this.justifyChildren;
        });
        allocationDX -= this.justifyChildren;
        this.dx = allocationDX < this.minDX ? this.minDX : allocationDX;
    }

    draw(offset = 0) {
        //draw this
        stroke(0);
        fill(this.colour === undefined ? _defaultBackgroundColour : this.colour);

        this.calculatedXpos = this.xPos;
        this.calculatedYpos = this.yPos;
        this.calculatedDx = this.dx;
        this.calculatedDy = this.dy;

        rect(this.xPos, this.yPos, this.dx, this.dy, this.radius);

        let xPos = this.xPos + this.padding.left;
        let yPos = this.yPos + this.padding.top;
        //draw children
        for (let i = 0; i < this.children.length; i++) {
            this.children[i].draw(xPos + (this.justifyChildren * i), yPos);
            xPos += this.children[i].dx;
        }
    }
}