const _defaultComponentRadius = 3;
const _defaultComponentColour = "#3e1a81";

class UIComponent extends IDraggable {
    //dimension
    dx;
    dy;
    //style
    radius;
    colour;
    justifyChildren = 0;
    //features
    stretchable = false;
    movable;

    //for optimization
    calculatedXpos;
    calculatedYpos;
    calculatedDy;
    calculatedDx;

    constructor(dx, dy, radius, colour, movable) {
        super();
        this.dx = dx;
        this.dy = dy;
        this.movable = movable ? movable : false;
        this.radius = radius === undefined ? _defaultComponentRadius : radius;
        this.colour = colour === undefined ? _defaultComponentColour : colour;

    }

    setMovable(state) {
        this.movable = state;
    }

    draw(x, y, ddx, ddy) {
        fill(this.colour);
        if (this.stretchable)
            this.doRect(x, y, ddx ? ddx : this.dx, ddy ? ddy : this.dy);
        else
            this.doRect(x, y, this.dx, this.dy);
    }

    doRect(x, y, dx, dy) {
        this.setCalculatedPosition(x, y, dx, dy);
        rect(x, y, dx, dy, this.radius);
    }

    setCalculatedPosition(x, y, dx, dy) {
        //for optimization purposes
        this.calculatedXpos = x;
        this.calculatedYpos = y;
        this.calculatedDx = dx;
        this.calculatedDy = dy;
    }

    drag() {
        if (isMouseInArea(this, mouseX, mouseY)) {
            if (this.movable) {
                let tempComp = this;
                curDraggingElm = setDraggingComp(tempComp, mouseX - this.calculatedXpos, mouseY - this.calculatedYpos);
                curDraggingElm.moveToXY(mouseX, mouseY);
            }
            return true;
        }
    }

    clone() {
        //leave movable;
        let newComp = new UIComponent(this.dx, this.dy, this.radius, this.colour, this.movable);
        newComp.setCalculatedPosition(this.calculatedXpos, this.calculatedYpos, this.calculatedDx, this.calculatedDy);
        return newComp;
    }
}