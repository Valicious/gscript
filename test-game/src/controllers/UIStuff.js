const UIComponents = [];

function UIStuff() {
    this.addAll = () => {
        let uiComp = new VBox(20, 30, 200, 70, 10);
        uiComp.padding = (new Padding()).setPadding(20);
        uiComp.justifyChildren = 20;

        uiComp.addChild(new UIComponent(50, 50, 5, "#ff00b7", true));
        uiComp.addChild(new UIComponent(70, 120, 5, "#ffc500", true));
        uiComp.addChild(new UIComponent(50, 100, 5, "rgb(0,231,255)", true));
        uiComp.addChild(new UIComponent(120, 50, 5, "#ff1600", true));

        UIComponents.push(uiComp);

        let instructionBar = new HBox(20, screenY - 180, 200, 100, 10);
        instructionBar.padding = (new Padding()).setPadding(20);
        instructionBar.justifyChildren = 20;
        instructionBar.setDropzone(new Dropzone());
        UIComponents.push(instructionBar)
    };

    this.draw = () => {
        UIComponents.forEach(cur => cur.draw())
    }
}
