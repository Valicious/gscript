let player1;

/**
 * @name Player Setup
 * @method Class Function
 *
 * Handles all the initialization of the player
 * location, skins, animation, movement, physics
 * @constructor
 * @author GA Aggenbach
 */
function playerController() {
    this.skins = {
        skinsList: [],
        curSkin: 0,
        frameDim: 0,
        curFrame: 0,
        skinsScale: 2
    };
    this._RefreshRate = 10;

    /**
     * @name Register player
     * @method createPlayer
     * @param {int} x - the initial x position
     * @param {int} y - the initial y poistion
     */
    this.createPlayer = function (x, y) {
        this.dx = 0;
        this.dy = 0;
        this.isJumping = false;
        player1 = new player(x, y);
        setSkins();
    };

    this.updatePlayer = function () {
        /* ANIMATION, SPRITE */
        let curSkin = skins.skinsList[skins.curSkin]
        if (frameCount % this._RefreshRate === 0)
            skins.curFrame++;
        if (skins.curFrame >= curSkin.frames)
            skins.curFrame = 0;
        // image(curSkin.img, 500,500, 200,200 , 100*skins.curFrame,0, 100,100);
        image(curSkin.img, player1.xPos, player1.yPos,
            skins.frameDim * skins.skinsScale, skins.frameDim * skins.skinsScale,
            skins.frameDim * skins.curFrame, 0, skins.frameDim, skins.frameDim);
        /* MOVEMENT */
        if (keyIsPressed)
            player1.xPos += this.dx;
        // player1.xPos += this.dy;
    }
}

/*
Private functions with some static methods
*/

/**
 * @name Player
 * @param {int} x - the initial x position
 * @param {int} y - the initial y poistion
 */
function player(x, y) {
    this.score = 0;
    this.xPos = x;
    this.yPos = y;

    /* Movement Controls STATIC*/
    moveLeft = function () {
        PC.dx = -2;
    };
    moveRight = function () {
        PC.dx = 2;
    };
    moveJump = function () {

    };

    playerCollisionCollider = function () {
        let isSafe = false;
        return isSafe;
    }
}

/**
 * @name Player Animation
 *
 * Loads the sprite sheets and sets up the animation rules.
 * @requires Idle should be set first for convenience.
 */
let setSkins = function () {
    skins = {//reset skins
        skinsList: [],
        curSkin: 0,
        frameDim: 100,
        curFrame: 0,
        skinsScale: 2
    };
    let skin_idle = {
        name: "Idle",
        img: loadImage('\\Assets\\player\\_idle.png'),
        frames: 3
    };
    let skin_runup = {
        name: "RunUp",
        img: loadImage('\\Assets\\player\\_runup.png'),
        frames: 4
    };
    let skin_run = {
        name: "Run",
        img: loadImage('\\Assets\\player\\_run.png'),
        frames: 6
    };
    skins.skinsList.push(skin_idle);
    skins.skinsList.push(skin_runup);
    skins.skinsList.push(skin_run);
}

/*
Added functions _________________________________________________________________________________________________________
*/

/**
 * @name Movement
 * @method movement
 *
 * Controls the character using keyboard input
 * @requires Keyboard input
 */
function movement() {
    if (key === 'a') {
        moveLeft();
    } else if (key === 'd') {
        moveRight();
    } else if (key === 'w') {
        moveJump();
    } else if (key === ' ') {
        //player.shootReady = true;
        moveJump();
    } else if (keyCode === LEFT_ARROW) {
        moveLeft();
    } else if (keyCode === RIGHT_ARROW) {
        moveRight();
    } else if (keyCode === UP_ARROW) {
        moveJump();
        //player.shootReady = true;
    }
}