const screenY = 750;
const screenX = 1000;
let PC;//*
let fps = 0;
let UIcomp;

function setup() {
    createCanvas(screenX, screenY);
    background(200);
    /*
    PC = new playerController();
    PC.createPlayer(400, 400);
    */
    UIcomp = new UIStuff();
    UIcomp.addAll();
}

function preload() {

}

function showFPScounter() {
    if (frameCount % 50 === 0)
        fps = floor(frameRate());
    fill(255, 0, 0);
    noStroke();
    textSize(20);
    textStyle(BOLD);
    text(fps, 10, 20);
}

function draw() {
    //reset canvas
    clear();
    background(200);

    //Draw ui
    UIcomp.draw();

    //Draw entities
    /*PC.updatePlayer();*/

    //Draw ui Dragging
    drawUIDragging();

    //test purposes
    this.showFPScounter();
}

/*

function keyTyped() {
    movement();
}

function keyPressed() {
    movement();
}
*/
