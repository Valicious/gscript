﻿using System.Collections.Generic;

namespace Construct_GScript {
    internal class State<G> : OnActionsHandler {
        private G value;

        public State(G value) {
            this.value = value;
        }

        public G get() {
            OnValueRequested(value);
            return value;
        }

        public void set(G value) {
            OnBeforeValueChanges(value);
            this.value = value;
            OnAfterValueChanges(value);
        }
    }
}