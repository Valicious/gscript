namespace Construct_GScript.modals
{
    internal interface Position
    {
        void MoveBodyTo(float xPos, float yPos);
        void MoveBody(int steps);
        void MoveBodyGraduallyTo(float xPos, float yPos);
        void RotateBody(int degree);
        void RotateBodyGradually(int degree);
    }
}