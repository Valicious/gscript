using System;

namespace Construct_GScript.modals
{
    internal abstract class Movement : Position
    {
        private State<float> currentXpos;
        private State<float> previousXpos;
        private State<float> currentYpos;
        private State<float> previousYpos;

        protected Movement(float initialXposition, float initialYposition)
        {
            setup(initialXposition, initialYposition);
        }

        private void setup(float initialXposition, float initialYposition)
        {
            currentXpos = new State<float>(initialXposition);
            currentYpos = new State<float>(initialYposition);
            previousXpos = new State<float>(-1f);
            previousYpos = new State<float>(-1f);

            currentXpos.ADD_OnAfterValueChanges((values) => previousXpos.set((float)values[0]));
            currentYpos.ADD_OnAfterValueChanges((values) => previousYpos.set((float)values[0]));

        }

        public void MoveBodyTo(float xPos, float yPos)
        {
            throw new System.NotImplementedException();
        }

        public void MoveBody(int steps)
        {
            throw new System.NotImplementedException();
        }

        public void MoveBodyGraduallyTo(float xPos, float yPos)
        {
            throw new System.NotImplementedException();
        }

        public void RotateBody(int degree)
        {
            throw new System.NotImplementedException();
        }

        public void RotateBodyGradually(int degree)
        {
            throw new System.NotImplementedException();
        }
    }
}