using Construct_GScript.src.events;
using System.Collections.Generic;

namespace Construct_GScript {
    internal abstract class OnActionsHandler {
        private List<GSEvent> _OnBeforeValueChanges;
        private List<GSEvent> _OnAfterValueChanges;
        private List<GSEvent> _OnValueRequested;

        protected internal void ADD_OnBeforeValueChanges(GSEvent newEvent) {
            Add(_OnBeforeValueChanges, newEvent);
        }

        protected internal void ADD_OnAfterValueChanges(GSEvent newEvent) {
            Add(_OnAfterValueChanges, newEvent);
        }

        protected internal void ADD_OnValueRequested(GSEvent newEvent) {
            Add(_OnValueRequested, newEvent);
        }

        protected void OnBeforeValueChanges(params object[] values) {
            Run(_OnBeforeValueChanges, values);
        }

        protected void OnAfterValueChanges(params object[] values) {
            Run(_OnAfterValueChanges, values);
        }

        protected void OnValueRequested(params object[] values) {
            Run(_OnValueRequested, values);
        }

        private void Add(List<GSEvent> listy, GSEvent newEvent) {
            listy.Add(newEvent);
        }


        private void Run(List<GSEvent> listy, params object[] values) {
            listy.ForEach(curElm => curElm());
        }
    }
}