﻿using System.Collections.Generic;

namespace Construct_GScript
{
    internal class GameCore
    {
        //UDD
        private IDictionary<char, IGSEvent> IOhandler;
        private IDictionary<string, AISystems> AIComponents;

        private IDictionary<string, State> States;
        private IDictionary<string, Prop> Props;
        private IDictionary<string, Prop> GameRules;

        private IDictionary<string, Environment> Evironments;
        private IDictionary<string, Entity> Entities;
        private IDictionary<string, Eventity> Eventities;


        
        public GameCore()
        {

        }

        public Environment InitializeNewEnvironment()
        {
            return new Environment();
        }

        public Entity InitializeNewEntity()
        {
            return new Entity();
        }
    }
}