//https://gist.github.com/alex-shamshurin/38610574bd9d8491d955401b032954b4

/**
 * Attaches [onBefore, onAfter, onRequest] event listeners to Object
 */
class OnActionsHandler {

    constructor() {
        if (this.constructor === OnActionsHandler)
            throw new TypeError("Cannot construct abstract class");
        this._OnBeforeValueChanges = [];
        this._OnAfterValueChanges = [];
        this._OnValueRequested = [];
    }

    set add_onBefore(event) {
        this._OnBeforeValueChanges.push(event);
    }

    set add_onAfter(event) {
        this._OnBeforeValueChanges.push(event);
    }

    set add_onRequest(event) {
        this._OnBeforeValueChanges.push(event);
    }

    run_onBefore(values) {
        this.run(this._OnBeforeValueChanges, values);
    }

    run_onAfter(values) {
        this.run(this._OnAfterValueChanges, values);
    }

    run_onRequest(values) {
        this.run(this._OnValueRequested, values);
    }

    run(list, values) {
        list.forEach(cur => cur.run(values))
    }

}