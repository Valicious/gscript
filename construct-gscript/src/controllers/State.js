class State extends OnActionsHandler {
    value;

    constructor(value) {
        super();
        this.value = value;
    }

    get value() {
        this.run_onRequest(value);
        return this.value;
    }

    set value(value) {
        this.run_onBefore(value);
        this.value = value;
        this.run_onAfter(value);
    }


}