class Movement {//abstract

    constructor() {
        if (this.constructor === Movement) {
            throw new TypeError("Can not construct class.");
        }
        //else (called from child)
        // Check if all instance methods are implemented.
        if (this.MoveBodyTo === Movement.prototype.MoveBodyTo) {
            throw new TypeError("Please implement method MoveBodyTo.");
        }
        if (this.MoveBody === Movement.prototype.MoveBody) {
            throw new TypeError("Please implement method MoveBody.");
        }
        if (this.MoveBodyGraduallyTo === Movement.prototype.MoveBodyGraduallyTo) {
            throw new TypeError("Please implement method MoveBodyGraduallyTo.");
        }
        if (this.RotateBody === Movement.prototype.RotateBody) {
            throw new TypeError("Please implement method RotateBody.");
        }
        if (this.RotateBodyGradually === Movement.prototype.RotateBodyGradually) {
            throw new TypeError("Please implement method RotateBodyGradually.");
        }
    }

    MoveBodyTo(xPos, yPos) {
        throw new TypeError("Do not call abstract method MoveBodyTo from child.");
    }

    MoveBody(steps) {
        throw new TypeError("Do not call abstract method MoveBody from child.");
    }

    MoveBodyGraduallyTo(xPos, yPos) {
        throw new TypeError("Do not call abstract method MoveBodyGraduallyTo from child.");
    }

    RotateBody(degrees) {
        throw new TypeError("Do not call abstract method RotateBody from child.");
    }

    RotateBodyGradually(degrees, steps) {
        throw new TypeError("Do not call abstract method RotateBodyGradually from child.");
    }

}