/**
 * Specifies the methods that any object must implement if they have coordinates and wish to to be translated or relocated or rotated.
 */
class Position {
    currentXpos;
    currentYpos;
    previousXpos;
    previousYpos;

    constructor(initialXpos, initialYpos) {


        this.setup(initialXpos, initialYpos);
    }


    setup(initialXpos, initialYpos) {
        this.currentXpos = new State(initialXpos);
        this.currentYpos = new State(initialYpos);
        this.previousXpos = new State(-1);
        this.previousYpos = new State(-1);

        this.currentXpos.add_onBefore(values => this.previousXpos.set(values[0]));
        this.currentYpos.add_onAfter(values => this.previousYpos.set(values[0]));
    }

    MoveBodyTo(xPos, yPos) {
        throw new TypeError("Do not call method MoveBodyTo from child.");
    }

    MoveBody(steps) {
        throw new TypeError("Do not call method MoveBody from child.");
    }

    MoveBodyGraduallyTo(xPos, yPos) {
        throw new TypeError("Do not call method MoveBodyGraduallyTo from child.");
    }

    RotateBody(degrees) {
        throw new TypeError("Do not call method RotateBody from child.");
    }

    RotateBodyGradually(degrees, steps) {
        throw new TypeError("Do not call method RotateBodyGradually from child.");
    }

}