class GameCore {
    IOhandler;
    AIComponents;

    States;
    Props;
    GameRules;

    Evironments;
    Entities;
    Eventities;


    constructor() {

    }

    /**
     * Create new environment
     * @returns {Environment}
     * @constructor
     */
    InitializeNewEnvironment = () => {
        return new Environment();
    };


    /**
     * Create new entity
     * @returns {Entity}
     * @constructor
     */
    InitializeNewEntity = () => {
        return new Entity();
    }
}
